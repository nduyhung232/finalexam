package com.example.demo.controller;

import com.example.demo.dao.BookSlipDAO;
import com.example.demo.dao.BookStatisticsDAO;
import com.example.demo.model.container.Properties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class Controller {

    @PostMapping("/borrowslip")
    public ResponseEntity<List> getBorrowSlip(@RequestBody Properties timeRequest){
        List list = BookStatisticsDAO.getBookList(timeRequest.getTimeFrom(), timeRequest.getTimeTo());
        return ResponseEntity.ok(list);
    }

    @PostMapping("/borrowslipdetail")
    public ResponseEntity<List> getBorrowSlipDetail(@RequestBody Properties properties){
        List list = BookSlipDAO.getBookDetail(properties);
        return ResponseEntity.ok(list);
    }
}
