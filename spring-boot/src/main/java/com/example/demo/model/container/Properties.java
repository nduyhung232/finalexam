package com.example.demo.model.container;

public class Properties {
  private String timeFrom;
  private String timeTo;
  private int idBook;

  public Properties(String timeFrom, String timeTo) {
    this.timeFrom = timeFrom;
    this.timeTo = timeTo;
  }

  public int getIdBook() {
    return idBook;
  }

  public void setIdBook(int idBook) {
    this.idBook = idBook;
  }

  public String getTimeFrom() {
    return timeFrom;
  }

  public void setTimeFrom(String timeFrom) {
    this.timeFrom = timeFrom;
  }

  public String getTimeTo() {
    return timeTo;
  }

  public void setTimeTo(String timeTo) {
    this.timeTo = timeTo;
  }
}
