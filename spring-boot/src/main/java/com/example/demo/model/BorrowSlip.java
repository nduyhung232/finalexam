package com.example.demo.model;

import java.util.Date;

public class BorrowSlip {
  private int id;
  private Reader reader;
  private Book book;
  private Date borrowDate;
  private Date returnDate;
  private int fine;
  private int totalBorrowings;

  public BorrowSlip(Date borrowDate, String readerOfName, Date returnDate, int fine) {
    Reader reader = new Reader(readerOfName);
    this.reader = reader;
    this.borrowDate = borrowDate;
    this.returnDate = returnDate;
    this.fine = fine;
  }

  public int getTotalBorrowings() {
    return totalBorrowings;
  }

  public void setTotalBorrowings(int totalBorrowings) {
    this.totalBorrowings = totalBorrowings;
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getBorrowDate() {
    return borrowDate;
  }

  public void setBorrowDate(Date borrowDate) {
    this.borrowDate = borrowDate;
  }

  public Reader getReader() {
    return reader;
  }

  public void setReader(Reader reader) {
    this.reader = reader;
  }

  public Date getReturnDate() {
    return returnDate;
  }

  public void setReturnDate(Date returnDate) {
    this.returnDate = returnDate;
  }

  public int getFine() {
    return fine;
  }

  public void setFine(int fine) {
    this.fine = fine;
  }
}
