package com.example.demo.model;

public class BookStatisticsTable extends Book {
    private int totalBorrowings;
    public BookStatisticsTable(int id, String name, String author, String barcode) {
        super(id, name, author, barcode);
    }

    public int getTotalBorrowings() {
        return totalBorrowings;
    }

    public void setTotalBorrowings(int totalBorrowings) {
        this.totalBorrowings = totalBorrowings;
    }
}
