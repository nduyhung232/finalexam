package com.example.demo.model;

import java.util.Date;

public class Book {
  private int id;
  private String name;
  private String author;
  private String publishingYear;
  private int price;
  private int quantity;
  private String description;
  private String barcode;

  public Book(int id, String name, String author, String barcode) {
    this.id = id;
    this.name = name;
    this.author = author;
    this.barcode = barcode;
  }

  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getPublishingYear() {
    return publishingYear;
  }

  public void setPublishingYear(String publishingYear) {
    this.publishingYear = publishingYear;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
