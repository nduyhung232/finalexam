package com.example.demo.dao;

import com.example.demo.model.BorrowSlip;
import com.example.demo.model.container.Properties;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookSlipDAO {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public static List<BorrowSlip> getBookDetail(Properties properties) {
        try {
            List<BorrowSlip> list = new ArrayList<>();

            Statement statement = connection.createStatement();
            String sql = "select a.borrowDate, b.name, a.returnDate, a.fine from borrowslip as a join reader as b\n" +
                    "on a.idReader = b.id where a.idbook = " + properties.getIdBook() + " and " +
                    "a.borrowDate between '" + properties.getTimeFrom() + "' and '" + properties.getTimeTo() + "'";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                BorrowSlip borrowSlip = new BorrowSlip(
                        resultSet.getDate(1),
                        resultSet.getString(2),
                        resultSet.getDate(3),
                        resultSet.getInt(4)
                );
                list.add(borrowSlip);
            }

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
