package com.example.demo.dao;

import com.example.demo.model.BookStatisticsTable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookStatisticsDAO {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public static List<BookStatisticsTable> getBookList(String timeFrom, String timeTo) {
        try {
            List<BookStatisticsTable> bookStatisticsTables = new ArrayList<>();

            Statement statement = connection.createStatement();
            String sql1 = "select distinct a.id, a.name, a.author, a.barcode from book as a join borrowslip as b \n" +
                "on a.id = b.idBook \n" +
                "where b.borrowDate between '" + timeFrom + "' and '" + timeTo + "'";
            ResultSet resultSet1 = statement.executeQuery(sql1);

            while (resultSet1.next()) {
                BookStatisticsTable bookStatisticsTable = new BookStatisticsTable(
                    resultSet1.getInt(1),
                    resultSet1.getString(2),
                    resultSet1.getString(3),
                    resultSet1.getString(4)
                );
                bookStatisticsTables.add(bookStatisticsTable);
            }

            for (BookStatisticsTable bookStatisticsTable : bookStatisticsTables) {
                Statement statement1 = connection.createStatement();
                String sql2 = "select count(idBook) from borrowslip as a \n" +
                    "where a.idBook = " + bookStatisticsTable.getId() + " and " +
                    "a.borrowDate between '" + timeFrom + "' and '" + timeTo + "'";
                ResultSet resultSet2 = statement1.executeQuery(sql2);

                if (resultSet2.next()) {
                    bookStatisticsTable.setTotalBorrowings(resultSet2.getInt(1));
                }
            }

            return bookStatisticsTables;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
