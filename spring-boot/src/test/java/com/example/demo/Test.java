package com.example.demo;

import com.example.demo.dao.BookStatisticsDAO;
import com.example.demo.model.container.Properties;
import org.junit.Assert;

public class Test {
    BookStatisticsDAO bookStatisticsDAO = new BookStatisticsDAO();

    @org.junit.Test
    public void Test1(){
        Properties properties = new Properties("2019-05-01", "2019-12-01");
        Assert.assertFalse(bookStatisticsDAO.getBookList(properties.getTimeFrom(), properties.getTimeTo()).isEmpty());
    }

    @org.junit.Test
    public void Test2(){
        Properties properties = new Properties("2020-05-01", "2020-12-01");
        Assert.assertFalse(bookStatisticsDAO.getBookList(properties.getTimeFrom(), properties.getTimeTo()).isEmpty());
    }
    @org.junit.Test
    public void Test3(){
        Properties properties = new Properties("", "");
        Assert.assertFalse(bookStatisticsDAO.getBookList(properties.getTimeFrom(), properties.getTimeTo()).isEmpty());
    }

    @org.junit.Test
    public void Test4(){
        Properties properties = new Properties("", "2019-12-01");
        Assert.assertFalse(bookStatisticsDAO.getBookList(properties.getTimeFrom(), properties.getTimeTo()).isEmpty());
    }

    @org.junit.Test
    public void Test5(){
        Properties properties = new Properties("2019-05-01", "");
        Assert.assertFalse(bookStatisticsDAO.getBookList(properties.getTimeFrom(), properties.getTimeTo()).isEmpty());
    }

    @org.junit.Test
    public void Test6(){
        Properties properties = new Properties("2020-05-01", "2019-12-01");
        Assert.assertFalse(bookStatisticsDAO.getBookList(properties.getTimeFrom(), properties.getTimeTo()).isEmpty());
    }
}
