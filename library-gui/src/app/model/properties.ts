export class  Properties {
  private timeFrom: string;
  private timeTo: string;
  private idBook: number;


  constructor(timeFrom: string, timeTo: string, idBook: number) {
    this.timeFrom = timeFrom;
    this.timeTo = timeTo;
    this.idBook = idBook;
  }
}
