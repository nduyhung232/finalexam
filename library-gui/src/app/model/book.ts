export class  Book {
  private id: number;
  private name: string;
  private author: string;
  private publishingYear: string;
  private price: number;
  private quantity: number;
  private description: string;
  private barcode: number;

  constructor(id: number, name: string, author: string, publishingYear: string, price: number, quantity: number, description: string, barcode: number) {
    this.id = id;
    this.name = name;
    this.author = author;
    this.publishingYear = publishingYear;
    this.price = price;
    this.quantity = quantity;
    this.description = description;
    this.barcode = barcode;
  }
}
