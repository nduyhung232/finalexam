import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuonsachComponent } from './muonsach.component';

describe('MuonsachComponent', () => {
  let component: MuonsachComponent;
  let fixture: ComponentFixture<MuonsachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuonsachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuonsachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
