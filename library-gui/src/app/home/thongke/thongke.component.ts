import {Component, OnInit} from '@angular/core';
import {LibraryService} from '../../library.service';
import {Properties} from '../../model/properties';
import {Borrowslip} from "../../model/borrowslip";

@Component({
  selector: 'app-thongke',
  templateUrl: './thongke.component.html',
  styleUrls: ['./thongke.component.scss']
})
export class ThongkeComponent implements OnInit {
  timeFrom: string;
  timeTo: string;
  bookList: any;
  bookDetail: any;
  sumString1: any;
  sumString2: any;
  sum1 = 0;
  sum2 = 0;

  constructor(private libraryService: LibraryService) {
  }

  ngOnInit() {
  }

  showBookList() {
    // @ts-ignore
    const time: Properties = {timeFrom: this.timeFrom, timeTo: this.timeTo};
    this.libraryService.showBorrowSlip(time).subscribe(
      data => {
        let data1: any;
        data1 = data;
        if (data1.length === 0) {
          alert('Danh Sach Khong Ton Tai');
        } else {
          let swap: any;
          for (let i = 0; i < data1.length - 1; i++) {
            for (let j = i + 1; j < data1.length; j++) {
              if (data[i].totalBorrowings < data[j].totalBorrowings) {
                swap = data[i];
                data[i] = data[j];
                data[j] = swap;
              }
            }
          }
          this.sum1 = 0;
          this.sumString1 = '';
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < data1.length; i++) {
            this.sum1 += data1[i].totalBorrowings;
          }
          this.sumString1 = 'Tong: ' + this.sum1;
          this.bookList = data;
          this.bookDetail = null;
        }
      });
  }

  showDetail(id: number) {
    // @ts-ignore
    const properties: Properties = {timeFrom: this.timeFrom, timeTo: this.timeTo, idBook: id};
    this.libraryService.showBorrowSlipDetail(properties).subscribe(
      data => {
        let data1: any;
        data1 = data;
        if (data1 === 0) {
          alert('Danh Sach Khong Ton Tai');
        } else {
          this.bookDetail = data;
          this.sum2 = 0;
          this.sumString2 = '';
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < data1.length; i++) {
            this.sum2 += data1[i].fine;
          }
          this.sumString2 = 'Tong: ' + this.sum2;
        }
      }
    );
  }
}
