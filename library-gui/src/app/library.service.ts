import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LinkServer} from './config';
import {Book} from "./model/book";
import {Properties} from "./model/properties";
import {Borrowslip} from "./model/borrowslip";

@Injectable({
  providedIn: 'root'
})
export class LibraryService {
  apiUrl = LinkServer.linkServer;

  constructor(private http: HttpClient) {
  }

  checkLogin(account) {
    return this.http.post<Account>(this.apiUrl + '/checklogin', account);
  }

  showBorrowSlip(properties: Properties) {
    return this.http.post<Book>(this.apiUrl + '/borrowslip', properties);
  }

  showBorrowSlipDetail(properties: Properties) {
    return this.http.post<Borrowslip>(this.apiUrl + '/borrowslipdetail', properties);
  }

}
