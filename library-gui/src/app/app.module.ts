import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { LibraryService } from './library.service';
import {HttpClientModule} from '@angular/common/http';
import { ThongkeComponent } from './home/thongke/thongke.component';
import { TrasachComponent } from './home/trasach/trasach.component';
import { MuonsachComponent } from './home/muonsach/muonsach.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ThongkeComponent,
    TrasachComponent,
    MuonsachComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [LibraryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
