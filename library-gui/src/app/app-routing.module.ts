import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ThongkeComponent } from './home/thongke/thongke.component';
import { MuonsachComponent } from './home/muonsach/muonsach.component';
import { TrasachComponent } from './home/trasach/trasach.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: ThongkeComponent,
      },
      {
        path: 'thongke',
        component: ThongkeComponent,
      },
      {
        path: 'muonsach',
        component: MuonsachComponent,
      },
      {
        path: 'trasach',
        component: TrasachComponent,
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
