-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: quanlygara
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chitietphieunhap`
--

DROP TABLE IF EXISTS `chitietphieunhap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitietphieunhap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLinhKien` int(11) NOT NULL,
  `idPhieuNhap` int(11) NOT NULL,
  `soLuong` int(11) NOT NULL,
  `tongTienLinhKien` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idLinhKien_idx` (`idLinhKien`),
  KEY `idPhieuNhap_idx` (`idPhieuNhap`),
  CONSTRAINT `idLinhKien` FOREIGN KEY (`idLinhKien`) REFERENCES `linhkien` (`id`),
  CONSTRAINT `idPhieuNhap` FOREIGN KEY (`idPhieuNhap`) REFERENCES `phieunhap` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietphieunhap`
--

LOCK TABLES `chitietphieunhap` WRITE;
/*!40000 ALTER TABLE `chitietphieunhap` DISABLE KEYS */;
INSERT INTO `chitietphieunhap` VALUES (32,1,9,3,300000),(33,2,9,4,400000),(34,3,9,5,500000),(35,4,9,6,600000),(36,5,10,2,200000),(37,6,10,1,100000),(38,1,10,3,300000),(39,2,10,4,400000),(40,4,11,5,500000),(41,8,11,6,600000),(42,9,11,7,700000),(43,12,12,3,300000),(44,1,12,2,200000),(45,2,13,1,100000),(46,3,14,3,300000),(47,4,14,33,3300000),(48,5,15,4,400000),(49,1,16,5,500000),(50,2,17,4,400000),(51,3,18,2,200000),(52,4,19,2,200000),(53,5,20,3,300000),(54,6,20,22,2200000),(55,10,20,2,200000),(56,11,21,4,400000),(57,1,21,4,400000),(58,2,21,2,200000),(59,6,22,1,100000),(60,8,22,1,100000),(61,7,22,1,100000),(62,5,22,1,100000),(63,1,1,2,200000),(64,1,1,2,200000),(65,1,1,3,300000),(66,2,2,3,300000),(67,2,2,3,300000),(68,2,3,4,400000),(69,1,4,4,400000),(70,3,5,4,400000),(71,4,6,1,100000),(72,5,7,1,100000),(73,6,7,1,100000),(74,8,8,1,100000),(75,7,8,3,300000),(76,9,8,3,300000);
/*!40000 ALTER TABLE `chitietphieunhap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linhkien`
--

DROP TABLE IF EXISTS `linhkien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `linhkien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenLinhKien` varchar(45) NOT NULL,
  `giaTien` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linhkien`
--

LOCK TABLES `linhkien` WRITE;
/*!40000 ALTER TABLE `linhkien` DISABLE KEYS */;
INSERT INTO `linhkien` VALUES (1,'Banh Xe',100000),(2,'Lop Xe',10000),(3,'Guong',10000),(4,'Vo Lang',10000),(5,'Ghe',10000),(6,'Xam Xe',100000),(7,'Can Gat Mua',100000),(8,'AcQuy',100000),(9,'Den Pha',100000),(10,'Den Xi Nhan',100000),(11,'Den Phanh',100000),(12,'Den Chieu Hau',100000),(13,'Giam Soc',100000),(14,'Day Phanh',100000),(15,'Dong Co',100000),(16,'Xi Lanh',100000),(17,'Bu Di',100000);
/*!40000 ALTER TABLE `linhkien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhacungcap`
--

DROP TABLE IF EXISTS `nhacungcap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nhacungcap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenNhaCungCap` varchar(45) NOT NULL,
  `diaChi` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `soDienThoai` varchar(45) NOT NULL,
  `moTa` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhacungcap`
--

LOCK TABLES `nhacungcap` WRITE;
/*!40000 ALTER TABLE `nhacungcap` DISABLE KEYS */;
INSERT INTO `nhacungcap` VALUES (1,'HonDa','NhatBan','HonDa@gmail.com','0123456',NULL),(2,'Toyota','NhatBan','Toyota@gmail.com','0789845',NULL),(3,'VinFast','VietNam','VinFast@gmail.com','0321748',NULL),(4,'Yamaha','NhatBan','Yamaha@gmail.com','0148792',NULL),(5,'Lexus','NhatBan','Lexus@gmail.com','0987466',NULL),(6,'Mercedes','Duc','Mercedes@gmail.com','0487958',NULL),(7,'Ford','My','Ford@gmail.com','0896526',NULL),(8,'Huyndai','HanQuoc','Huyndai@gmail.com','0545784',NULL),(9,'Ferrari','Duc','Ferrari@gmail.com','0892444',NULL),(10,'Lamborghini','My','Lamborghini@gmail.com','0154988',NULL);
/*!40000 ALTER TABLE `nhacungcap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phieunhap`
--

DROP TABLE IF EXISTS `phieunhap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phieunhap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idNhaCungCap` int(11) NOT NULL,
  `ngayNhap` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idNhaCungCap_idx` (`idNhaCungCap`),
  CONSTRAINT `idNhaCungCap` FOREIGN KEY (`idNhaCungCap`) REFERENCES `nhacungcap` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phieunhap`
--

LOCK TABLES `phieunhap` WRITE;
/*!40000 ALTER TABLE `phieunhap` DISABLE KEYS */;
INSERT INTO `phieunhap` VALUES (1,2,'2019-05-15'),(2,1,'2019-05-19'),(3,3,'2019-03-15'),(4,4,'2019-01-15'),(5,5,'2019-02-15'),(6,6,'2019-03-15'),(7,7,'2019-04-15'),(8,8,'2019-05-15'),(9,5,'2019-06-15'),(10,6,'2019-07-15'),(11,8,'2019-08-15'),(12,7,'2019-09-15'),(13,4,'2019-10-15'),(14,1,'2019-11-15'),(15,2,'2019-12-15'),(16,3,'2019-05-15'),(17,1,'2019-05-15'),(18,1,'2019-05-15'),(19,2,'2019-06-15'),(20,3,'2019-07-15'),(21,4,'2019-08-15'),(22,5,'2019-09-15'),(23,1,'2019-05-15');
/*!40000 ALTER TABLE `phieunhap` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-15 17:44:47
