package com.example.demo;

import com.example.demo.dao.ThongKeNCCDAO;
import com.example.demo.model.contain.Properties;
import org.junit.Assert;


public class Test {
    ThongKeNCCDAO thongKeNCCDAO = new ThongKeNCCDAO();

    @org.junit.Test
    public void Test1() {
        Properties properties = new Properties("2019-05-01", "2019-12-01");
        Assert.assertFalse(thongKeNCCDAO.getNhaCungCapTable(properties.getNgayBatDau(), properties.getNgayKetThuc()).isEmpty());
    }

    @org.junit.Test
    public void Test2() {
        Properties properties = new Properties("2020-05-01", "2019-12-01");
        Assert.assertTrue(thongKeNCCDAO.getNhaCungCapTable(properties.getNgayBatDau(), properties.getNgayKetThuc()).isEmpty());
    }

    @org.junit.Test
    public void Test3() {
        Properties properties = new Properties("2020-05-01", "2020-12-01");
        Assert.assertTrue(thongKeNCCDAO.getNhaCungCapTable(properties.getNgayBatDau(), properties.getNgayKetThuc()).isEmpty());
    }

    @org.junit.Test
    public void Test4() {
        Properties properties = new Properties("", "");
        Assert.assertTrue(thongKeNCCDAO.getNhaCungCapTable(properties.getNgayBatDau(), properties.getNgayKetThuc()).isEmpty());
    }

    @org.junit.Test
    public void Test5() {
        Properties properties = new Properties("", "2019-12-01");
        Assert.assertFalse(thongKeNCCDAO.getNhaCungCapTable(properties.getNgayBatDau(), properties.getNgayKetThuc()).isEmpty());
    }

    @org.junit.Test
    public void Test6() {
        Properties properties = new Properties("2019-05-01", "");
        Assert.assertTrue(thongKeNCCDAO.getNhaCungCapTable(properties.getNgayBatDau(), properties.getNgayKetThuc()).isEmpty());
    }
}
