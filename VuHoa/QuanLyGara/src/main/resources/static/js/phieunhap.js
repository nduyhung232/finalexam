$(document).ready(function () {
    // set title
    var title = localStorage.getItem("tenNhaCungCap");
    $('#title').html("Danh Sách Các Phiếu Nhập Của " + title);

    var timeStart = localStorage.getItem("timeStart");
    var timeEnd = localStorage.getItem("timeEnd");
    var idNhaCungCap = localStorage.getItem("idNhaCungCap");

    $.ajax({
        url: '/getphieunhap',
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: 'application/json',
        data: JSON.stringify({
            ngayBatDau: timeStart,
            ngayKetThuc: timeEnd,
            idNhaCungCap: idNhaCungCap
        }),

        success: function (data) {
            if (data.length == 0) {
                alert("Danh Sach Khong Ton Tai");
            } else {
                $("#phieunhap-body").empty();
                var totalMoney = 0;

                // Sắp xếp phiếu nhập theo tổng số tiền từ nhiều đến ít
                var swap;
                for (var i = 0; i < data.length - 1; i++) {
                    for (var j = i + 1; j < data.length; j++) {
                        if (data[i].tongTien < data[j].tongTien) {
                            swap = data[i].tongTien;
                            data[i].tongTien = data[j].tongTien;
                            data[j].tongTien = swap;
                        }
                    }
                }

                for (var i = 0; i < data.length; i++) {
                    $("#phieunhap-body").append(
                        "<tr>\n" +
                        "        <td style='display: none' id='idPhieuNhap'>" + data[i].id + "</td>\n" +
                        "        <td>" + data[i].ngayNhap + "</td>\n" +
                        "        <td>" + data[i].tongSoLuong + "</td>\n" +
                        "        <td>" + data[i].tongTien + "</td>\n" +
                        "    </tr>"
                    );
                    totalMoney += data[i].tongTien;
                }

                $("#phieunhap-body").append(
                    "<tr>\n" +
                    "        <td>Tổng Tiền:</td>\n" +
                    "        <td>" + totalMoney + "</td>\n" +
                    "    </tr>"
                );
            }
        },
        error: function (data) {
            alert(data)
        }
    });

    $('#phieunhap').on('click', 'tbody tr', function (event) {
        if ($(this).find("#idPhieuNhap").text() == "")
            return;

        var idPhieuNhap = $(this).find("#idPhieuNhap").text();
        localStorage.setItem("idPhieuNhap", idPhieuNhap);

        window.location.href = '/phieunhapchitiet.html';
    });

    $('#btn-back').click(function () {
        window.location.href = '/thongke.html';
    });

    $('#btn-exit').click(function () {
        window.location.href = '/home.html';
    });
});
