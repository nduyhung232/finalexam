$(document).ready(function () {

    var idPhieuNhap = localStorage.getItem("idPhieuNhap");
    var tenHang = localStorage.getItem("tenNhaCungCap");

    var idNhaCungCap = localStorage.getItem("idNhaCungCap");
    var tenNhaCungCap = localStorage.getItem("tenNhaCungCap");
    var diaChi = localStorage.getItem("diaChi");
    var enail = localStorage.getItem("email");
    var soDT = localStorage.getItem("soDT");
    var moTa = localStorage.getItem("moTa");

    $("#ncc-body").append(
        "<tr>\n" +
        "        <td>" + idNhaCungCap + "</td>\n" +
        "        <td>" + tenNhaCungCap + "</td>\n" +
        "        <td>" + diaChi + "</td>\n" +
        "        <td>" + enail + "</td>\n" +
        "        <td>" + soDT + "</td>\n" +
        "        <td>" + moTa + "</td>\n" +
        "    </tr>"
    );

    $.ajax({
        url: '/getchitietphieunhap',
        dataType: 'json',
        type: 'POST',
        cache: false,
        contentType: 'application/json',
        data: JSON.stringify({
            idPhieuNhap: idPhieuNhap
        }),

        success: function (data) {
            if (data.length == 0) {
                alert("Danh Sach Khong Ton Tai");
            } else {
                $("#phieunhapchitiet-body").empty();
                var totalMoney = 0;
                for (var i = 0; i < data.length; i++) {
                    $("#phieunhapchitiet-body").append(
                        "<tr>\n" +
                        "        <td>" + data[i].linhKien.id + "</td>\n" +
                        "        <td>" + data[i].linhKien.tenLinhKien + "</td>\n" +
                        "        <td>" + tenHang + "</td>\n" +
                        "        <td>" + data[i].linhKien.giaTien + "</td>\n" +
                        "        <td>" + data[i].soLuong + "</td>\n" +
                        "        <td>" + data[i].tongTienLinhKien + "</td>\n" +
                        "    </tr>"
                    );
                    totalMoney += data[i].tongTienLinhKien;
                }

                $("#phieunhapchitiet-body").append(
                    "<tr>\n" +
                    "        <td>Tổng Tiền:</td>\n" +
                    "        <td>" + totalMoney + "</td>\n" +
                    "    </tr>"
                );
            }
        },
        error: function (data) {
            alert(data)
        }
    });

    $('#btn-back').click(function () {
        window.location.href = '/phieunhap.html';
    });

    $('#btn-exit').click(function () {
        window.location.href = '/home.html';
    })
});
