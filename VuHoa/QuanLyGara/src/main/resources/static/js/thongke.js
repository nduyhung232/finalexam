$(document).ready(function () {
    $('#ncc').on('click', 'tbody tr', function (event) {
        if ($(this).find("#tenNhaCungCap").text() == "")
            return;

        var idNhaCungCap = $(this).find("#idNhaCungCap").text();
        var tenNhaCungCap = $(this).find("#tenNhaCungCap").text();
        var diaChi = $(this).find("#diaChi").text();
        var enail = $(this).find("#email").text();
        var soDT = $(this).find("#soDT").text();
        var moTa = $(this).find("#mota").text();

        localStorage.setItem("idNhaCungCap", idNhaCungCap);
        localStorage.setItem("tenNhaCungCap", tenNhaCungCap);
        localStorage.setItem("diaChi", diaChi);
        localStorage.setItem("email", enail);
        localStorage.setItem("soDT", soDT);
        localStorage.setItem("moTa", moTa);

        window.location.href = '/phieunhap.html';
    });

    $('#btn-back').click(function () {
        window.location.href = '/home.html';
    });

    $('#btn-thongke').click(function () {
        var timeStart = $("#time-start").val();
        var timeEnd = $("#time-end").val();

        localStorage.setItem("timeStart", timeStart);
        localStorage.setItem("timeEnd", timeEnd);

        $.ajax({
            url: '/getnhacungcap',
            dataType: 'json',
            type: 'POST',
            cache: false,
            contentType: 'application/json',
            data: JSON.stringify({
                ngayBatDau: timeStart,
                ngayKetThuc: timeEnd
            }),

            success: function (data) {
                if (data.length == 0) {
                    alert("Danh Sach Khong Ton Tai");
                } else {
                    $("#ncc-body").empty();
                    var totalMoney = 0;

                    // Sắp xếp nhà cung cấp theo doanh chi từ nhiều đến ít
                    var swap;
                    for (var i = 0; i < data.length - 1; i++) {
                        for (var j = i + 1; j < data.length; j++) {
                            if (data[i].doanhChi < data[j].doanhChi) {
                                swap = data[i].doanhChi;
                                data[i].doanhChi = data[j].doanhChi;
                                data[j].doanhChi = swap;
                            }
                        }
                    }

                    for (var i = 0; i < data.length; i++) {
                        $("#ncc-body").append(
                            " <tr>\n" +
                            "        <td id='idNhaCungCap'>" + data[i].id + "</td>\n" +
                            "        <td id='tenNhaCungCap'>" + data[i].tenNhaCungCap + "</td>\n" +
                            "        <td id='diaChi'>" + data[i].diaChi + "</td>\n" +
                            "        <td style='display: none' id='email'>" + data[i].email + "</td>\n" +
                            "        <td style='display: none' id='soDT'>" + data[i].soDienThoai + "</td>\n" +
                            "        <td style='display: none' id='moTa'>" + data[i].moTa + "</td>\n" +
                            "        <td>" + data[i].tongSoLinhKien + "</td>\n" +
                            "        <td>" + data[i].doanhChi + "</td>\n" +
                            "    </tr>"
                        );
                        totalMoney += data[i].doanhChi;
                    }

                    $("#ncc-body").append(
                        "<tr>\n" +
                        "        <td>Tổng Tiền:</td>\n" +
                        "        <td>" + totalMoney + "</td>\n" +
                        "    </tr>"
                    );
                }
            },
            error: function (data) {
                alert(data)
            }
        })
    });
})

