package com.example.demo.dao;

import com.example.demo.model.ChiTietPhieuNhap;
import com.example.demo.model.contain.NhaCungCapTable;
import com.example.demo.model.PhieuNhap;
import com.example.demo.model.contain.Properties;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PhieuNhapDAO {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public List<PhieuNhap> getPhieuNhapTable(Properties properties) {
        List<PhieuNhap> phieuNhapList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();

            /* Lấy thông tin id và ngày của tất cả các phiếu nhập nằm trong khoảng thời gian trên */

            String sql1 = "select id, ngayNhap from phieunhap " +
                    "where idNhaCungCap = " + properties.getIdNhaCungCap() + " and " +
                    "ngayNhap between '" + properties.getNgayBatDau() + "' and '" + properties.getNgayKetThuc() + "'";
            ResultSet resultSet1 = statement.executeQuery(sql1);

            while (resultSet1.next()) {
                PhieuNhap phieuNhap = new PhieuNhap(
                        resultSet1.getInt(1),
                        resultSet1.getString(2)
                );

                phieuNhapList.add(phieuNhap);
            }

             /* Lấy thông tin về tổng số lượng và tổng doanh chi của các phiếu nhập trong
                 khoảng thời gian trên*/

            for (PhieuNhap phieuNhap : phieuNhapList) {
                String sql2 = "select SUM(soLuong), SUM(tongTienLinhKien) from chitietphieunhap " +
                        "where idPhieuNhap = " + phieuNhap.getId();
                ResultSet resultSet2 = statement.executeQuery(sql2);

                if (resultSet2.next()) {
                    phieuNhap.setTongSoLuong(resultSet2.getInt(1));
                    phieuNhap.setTongTien(resultSet2.getInt(2));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return phieuNhapList;
    }

    public List<ChiTietPhieuNhap> getChiTietPhieuNhapTable(Properties properties) {
        List<ChiTietPhieuNhap> chiTietPhieuNhapList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();

            /* Lấy thông tin id, tenLinhKien, giaTien, soLuong, tongTienLinhKien Trong phiếu nhập trên */

            String sql1 = "select a.*, b.soLuong, b.tongTienLinhKien from linhkien as a join chitietphieunhap as b " +
                    "on a.id = b.idLinhKien " +
                    "where b.idPhieuNhap = " + properties.getIdPhieuNhap();
            ResultSet resultSet1 = statement.executeQuery(sql1);

            while (resultSet1.next()) {
                ChiTietPhieuNhap chiTietPhieuNhap = new ChiTietPhieuNhap(
                        resultSet1.getInt(1),
                        resultSet1.getString(2),
                        resultSet1.getInt(3),
                        resultSet1.getInt(4),
                        resultSet1.getInt(5)
                );

                chiTietPhieuNhapList.add(chiTietPhieuNhap);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chiTietPhieuNhapList;
    }
}
