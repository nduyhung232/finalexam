package com.example.demo.controller;

import com.example.demo.dao.PhieuNhapDAO;
import com.example.demo.dao.ThongKeNCCDAO;
import com.example.demo.model.ChiTietPhieuNhap;
import com.example.demo.model.contain.Properties;
import com.example.demo.model.contain.NhaCungCapTable;
import com.example.demo.model.PhieuNhap;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RestAPI {
  PhieuNhapDAO phieuNhapDAO = new PhieuNhapDAO();
  ThongKeNCCDAO thongKeNCCDAO = new ThongKeNCCDAO();

  @PostMapping("/getnhacungcap")
  public ResponseEntity getNhaCungCapTable(@RequestBody Properties properties){
    List<NhaCungCapTable> nhaCungCapTableList = new ArrayList<>();
    nhaCungCapTableList.addAll(thongKeNCCDAO.getNhaCungCapTable(properties.getNgayBatDau(), properties.getNgayKetThuc()));
    return ResponseEntity.ok(nhaCungCapTableList);
  }

  @PostMapping("/getphieunhap")
  public ResponseEntity getPhieunNhap(@RequestBody Properties properties){
    List<PhieuNhap> phieuNhapList = new ArrayList<>();
    phieuNhapList.addAll(phieuNhapDAO.getPhieuNhapTable(properties));
    return ResponseEntity.ok(phieuNhapList);
  }

  @PostMapping("/getchitietphieunhap")
  public ResponseEntity getChiTietPhieunNhap(@RequestBody Properties properties){
    List<ChiTietPhieuNhap> chiTietPhieuNhapList = new ArrayList<>();
    chiTietPhieuNhapList.addAll(phieuNhapDAO.getChiTietPhieuNhapTable(properties));
    return ResponseEntity.ok(chiTietPhieuNhapList);
  }
}
