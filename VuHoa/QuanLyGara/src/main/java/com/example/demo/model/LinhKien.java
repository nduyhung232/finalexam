package com.example.demo.model;

public class LinhKien {
  private int id;
  private String tenLinhKien;
  private int giaTien;

  public LinhKien(int id, String tenLinhKien, int giaTien) {
    this.id = id;
    this.tenLinhKien = tenLinhKien;
    this.giaTien = giaTien;
  }

  public LinhKien() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTenLinhKien() {
    return tenLinhKien;
  }

  public void setTenLinhKien(String tenLinhKien) {
    this.tenLinhKien = tenLinhKien;
  }

  public int getGiaTien() {
    return giaTien;
  }

  public void setGiaTien(int giaTien) {
    this.giaTien = giaTien;
  }
}
