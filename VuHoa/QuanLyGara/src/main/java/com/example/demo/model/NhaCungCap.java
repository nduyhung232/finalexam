package com.example.demo.model;

public class NhaCungCap {
  private int id;
  private String tenNhaCungCap;
  private String diaChi;
  private String email;
  private String soDienThoai;
  private String moTa;

  public NhaCungCap() {
  }

  public NhaCungCap(int id, String tenNhaCungCap, String diaChi, String email, String soDienThoai, String moTa) {
    this.id = id;
    this.tenNhaCungCap = tenNhaCungCap;
    this.diaChi = diaChi;
    this.email = email;
    this.soDienThoai = soDienThoai;
    this.moTa = moTa;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTenNhaCungCap() {
    return tenNhaCungCap;
  }

  public void setTenNhaCungCap(String tenNhaCungCap) {
    this.tenNhaCungCap = tenNhaCungCap;
  }

  public String getDiaChi() {
    return diaChi;
  }

  public void setDiaChi(String diaChi) {
    this.diaChi = diaChi;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSoDienThoai() {
    return soDienThoai;
  }

  public void setSoDienThoai(String soDienThoai) {
    this.soDienThoai = soDienThoai;
  }

  public String getMoTa() {
    return moTa;
  }

  public void setMoTa(String moTa) {
    this.moTa = moTa;
  }
}
