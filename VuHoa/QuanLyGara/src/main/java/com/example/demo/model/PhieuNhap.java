package com.example.demo.model;

import com.example.demo.model.NhaCungCap;

import java.util.Date;

public class PhieuNhap {
    private int id;
    private NhaCungCap nhaCungCap;
    private String ngayNhap;
    private int tongSoLuong;
    private int tongTien;

    public PhieuNhap(int id, String ngayNhap) {
        this.id = id;
        this.ngayNhap = ngayNhap;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NhaCungCap getNhaCungCap() {
        return nhaCungCap;
    }

    public void setNhaCungCap(NhaCungCap nhaCungCap) {
        this.nhaCungCap = nhaCungCap;
    }

    public int getTongSoLuong() {
        return tongSoLuong;
    }

    public void setTongSoLuong(int tongSoLuong) {
        this.tongSoLuong = tongSoLuong;
    }

    public int getTongTien() {
        return tongTien;
    }

    public void setTongTien(int tongTien) {
        this.tongTien = tongTien;
    }

    public PhieuNhap() {
    }
}
