package com.example.demo.model;

public class ChiTietPhieuNhap {
  private int id;
  private LinhKien linhKien;
  private PhieuNhap phieuNhap;
  private int soLuong;
  private int tongTienLinhKien;

  public ChiTietPhieuNhap(int maLinhKien, String tenLinhKien, int donGia, int soLuong, int tongTienLinhKien) {
    LinhKien linhKien1 = new LinhKien(maLinhKien, tenLinhKien, donGia);

    this.linhKien = linhKien1;
    this.soLuong = soLuong;
    this.tongTienLinhKien = tongTienLinhKien;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public LinhKien getLinhKien() {
    return linhKien;
  }

  public void setLinhKien(LinhKien linhKien) {
    this.linhKien = linhKien;
  }

  public PhieuNhap getPhieuNhap() {
    return phieuNhap;
  }

  public void setPhieuNhap(PhieuNhap phieuNhap) {
    this.phieuNhap = phieuNhap;
  }

  public int getSoLuong() {
    return soLuong;
  }

  public void setSoLuong(int soLuong) {
    this.soLuong = soLuong;
  }

  public int getTongTienLinhKien() {
    return tongTienLinhKien;
  }

  public void setTongTienLinhKien(int tongTienLinhKien) {
    this.tongTienLinhKien = tongTienLinhKien;
  }

}
