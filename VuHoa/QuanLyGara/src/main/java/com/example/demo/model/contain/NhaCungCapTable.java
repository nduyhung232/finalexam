package com.example.demo.model.contain;

import com.example.demo.model.NhaCungCap;

public class NhaCungCapTable extends NhaCungCap{
  private int tongSoLinhKien;
  private int doanhChi;

  public NhaCungCapTable(int id, String tenNhaCungCap, String diaChi, String email, String soDT, String moTa) {
    super(id, tenNhaCungCap, diaChi, email, soDT, moTa);
    this.tongSoLinhKien = 0;
    this.doanhChi = 0;
  }

  public int getTongSoLinhKien() {
    return tongSoLinhKien;
  }

  public void setTongSoLinhKien(int tongSoLinhKien) {
    this.tongSoLinhKien = tongSoLinhKien;
  }

  public int getDoanhChi() {
    return doanhChi;
  }

  public void setDoanhChi(int doanhChi) {
    this.doanhChi = doanhChi;
  }
}
