package com.example.demo.dao;

import com.example.demo.model.contain.NhaCungCapTable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ThongKeNCCDAO {
    static MyConnectionSql myConnectionSQL = new MyConnectionSql();
    public static Connection connection = myConnectionSQL.getConnection();

    public List<NhaCungCapTable> getNhaCungCapTable(String ngayBatDau, String ngayKetThuc) {
        List<NhaCungCapTable> nhaCungCapTableList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();

            /* Lấy thông tin của tất cả các nhà cung cấp có phiếu nhập trong khoảng thời gian
                từ ngayBatDau đến ngayKetThuc*/

            String sql1 = "select * from nhacungcap " +
                    "where id in (select idNhaCungCap from  phieunhap " +
                    "where ngayNhap between '" + ngayBatDau + "' and '" + ngayKetThuc + "')";
            ResultSet resultSet1 = statement.executeQuery(sql1);

            while (resultSet1.next()) {
                NhaCungCapTable nhaCungCapTable = new NhaCungCapTable(
                        resultSet1.getInt(1),
                        resultSet1.getString(2),
                        resultSet1.getString(3),
                        resultSet1.getString(4),
                        resultSet1.getString(5),
                        resultSet1.getString(6)
                );

                nhaCungCapTableList.add(nhaCungCapTable);
            }

            /* Lấy thông tin về tổng số lượng và tổng doanh chi của các nhà cung cấp trong
                 khoảng thời gian trên*/

            for (NhaCungCapTable nhaCungCapTable : nhaCungCapTableList) {
                String sql2 = "select SUM(soLuong), SUM(tongTienLinhKien) from chitietphieunhap " +
                        "where idPhieuNhap in (select id from phieunhap " +
                        "where idNhaCungCap = " + nhaCungCapTable.getId() +
                        " and ngayNhap between '" + ngayBatDau + "' and '" + ngayKetThuc + "')";
                ResultSet resultSet2 = statement.executeQuery(sql2);

                if (resultSet2.next()) {
                    nhaCungCapTable.setTongSoLinhKien(resultSet2.getInt(1));
                    nhaCungCapTable.setDoanhChi(resultSet2.getInt(2));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nhaCungCapTableList;
    }
}
