-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `author` varchar(45) NOT NULL,
  `publishingYear` varchar(45) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `barcode` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Toán','NXB Giáo Dục','2010',10000,20000,NULL,'gfh5T5'),(2,'Văn','NXB Giáo Dục','2010',10000,20000,NULL,'t5TDV5'),(3,'Vật Lý','NXB Giáo Dục','2010',10000,20000,NULL,'hi86Db'),(4,'Hóa Học','NXB Giáo Dục','2010',10000,20000,NULL,'tfr3dg'),(5,'Sinh Học','NXB Giáo Dục','2010',10000,20000,NULL,'tjjnfr'),(6,'Lịch Sử','NXB Giáo Dục','2010',12000,20000,NULL,'tt567j'),(7,'Địa Lý','NXB Giáo Dục','2010',12000,20000,NULL,'mczarh'),(8,'Tiếng Anh','NXB Giáo Dục','2010',12000,20000,NULL,'9ijryd'),(9,'Thể Dục','NXB Giáo Dục','2010',12000,20000,NULL,'juiikf'),(10,'Giáo Dục Công Dân','NXB Giáo Dục','2010',12000,20000,NULL,'09oi8r'),(11,'Khoa Học & Xã Hội','NXB Giáo Dục','2010',15000,20000,NULL,'4deeof'),(12,'Công Nghệ','NXB Giáo Dục','2010',15000,20000,NULL,'ceds7u'),(13,'Tin Học','NXB Giáo Dục','2010',15000,20000,NULL,'grLksw'),(14,'Giáo Dục Quốc Phòng','NXB Giáo Dục','2010',15000,20000,NULL,'0lkc98');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrowslip`
--

DROP TABLE IF EXISTS `borrowslip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `borrowslip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idReader` int(11) NOT NULL,
  `idBook` int(11) NOT NULL,
  `borrowDate` date NOT NULL,
  `returnDate` date NOT NULL,
  `fine` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idBook_idx` (`idBook`),
  KEY `idReader_idx` (`idReader`),
  CONSTRAINT `idBook` FOREIGN KEY (`idBook`) REFERENCES `book` (`id`),
  CONSTRAINT `idReader` FOREIGN KEY (`idReader`) REFERENCES `reader` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrowslip`
--

LOCK TABLES `borrowslip` WRITE;
/*!40000 ALTER TABLE `borrowslip` DISABLE KEYS */;
INSERT INTO `borrowslip` VALUES (1,1,1,'2019-01-01','2019-01-10',0),(2,2,1,'2019-01-02','2019-01-05',0),(3,4,1,'2019-01-03','2019-01-19',0),(4,1,1,'2019-01-04','2019-01-17',0),(5,5,1,'2019-01-05','2019-01-29',0),(6,6,1,'2019-01-06','2019-02-23',4000),(7,3,2,'2019-02-01','2019-02-25',0),(8,2,2,'2019-02-02','2019-02-06',0),(9,1,2,'2019-02-03','2019-02-14',0),(10,5,2,'2019-02-04','2019-02-11',0),(11,4,3,'2019-03-06','2019-03-06',0),(12,1,3,'2019-03-06','2019-11-30',4000),(13,1,3,'2019-03-06','2019-11-30',4000),(14,1,3,'2019-03-06','2019-11-30',4000),(15,2,3,'2019-03-06','2019-11-30',4000),(16,2,4,'2019-04-06','2019-11-30',4000),(17,2,4,'2019-04-06','2019-11-30',4000),(18,6,4,'2019-04-06','2019-11-30',4000),(19,3,4,'2019-04-06','2019-11-30',4000),(20,2,4,'2019-04-06','2019-11-30',4000),(21,5,5,'2019-05-06','2019-11-30',4000),(22,4,5,'2019-05-06','2019-11-30',4000),(23,1,5,'2019-05-06','2019-11-30',4000),(24,2,5,'2019-05-06','2019-11-30',4000),(25,5,5,'2019-05-06','2019-11-30',4000),(26,5,6,'2019-06-06','2019-11-30',4000),(27,5,6,'2019-06-06','2019-11-30',4000),(28,6,6,'2019-06-06','2019-11-30',4000),(29,3,6,'2019-06-06','2019-11-30',4000),(30,2,6,'2019-06-06','2019-11-30',4000),(31,6,7,'2019-07-06','2019-11-30',4000),(32,5,7,'2019-07-06','2019-11-30',4000),(33,4,7,'2019-07-06','2019-11-30',4000),(34,1,7,'2019-07-06','2019-11-30',4000),(35,2,7,'2019-07-06','2019-11-30',4000),(36,3,7,'2019-07-06','2019-11-30',4000),(37,2,7,'2019-07-06','2019-11-30',4000),(38,1,8,'2019-08-06','2019-11-30',4000),(39,4,8,'2019-08-06','2019-11-30',4000),(40,5,8,'2019-08-06','2019-11-30',4000),(41,4,8,'2019-08-06','2019-11-30',4000),(42,1,8,'2019-08-06','2019-11-30',4000),(43,1,8,'2019-08-06','2019-11-30',4000),(44,1,9,'2019-09-06','2019-11-30',4000),(45,5,9,'2019-09-06','2019-11-30',4000),(46,5,9,'2019-09-06','2019-11-30',4000),(47,5,9,'2019-09-06','2019-11-30',4000),(48,6,10,'2019-10-06','2019-11-30',4000),(49,6,10,'2019-10-06','2019-11-30',4000),(50,6,10,'2019-10-06','2019-12-30',4000),(51,3,11,'2019-11-06','2019-12-30',4000),(52,3,11,'2019-11-06','2019-12-30',4000),(53,3,12,'2019-12-06','2019-12-30',0),(54,2,12,'2019-12-06','2019-12-30',0),(55,5,12,'2019-12-06','2019-12-30',0);
/*!40000 ALTER TABLE `borrowslip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reader`
--

DROP TABLE IF EXISTS `reader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `dateOfBirth` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  `barCode` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reader`
--

LOCK TABLES `reader` WRITE;
/*!40000 ALTER TABLE `reader` DISABLE KEYS */;
INSERT INTO `reader` VALUES (1,'Nguyễn Duy Hưng','23/02/1997','Thanh Hóa','0978144871','aeF8Gfgg'),(2,'Dương Văn Hùng','15/04/1984','Hà Nội','0984236521','sasI8sSkw'),(3,'Trần Xuân Quang','22/10/1993','Thái Bình','0912455421','swJWJSw2'),(4,'Nguyễn An Anh','06/08/1992','Hà Nội','0985412236','afjUs3fr'),(5,'Đỗ Duy Linh','25/12/1995','Hà Nội','0978555412','87hKKAN2'),(6,'Phan Văn Hùng','14/11/1993','Vĩnh Phúc','0932516478','dic7Sw33');
/*!40000 ALTER TABLE `reader` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-17 19:12:42
